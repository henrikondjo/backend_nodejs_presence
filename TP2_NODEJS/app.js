const express = require('express');
//variable
const app = express();
const port = 5000;
//notre point d'entrer
app.get('/', (req, res) => {
    res.send('Initial point')
})
//body parser
app.use(express.json());
//internal imports
const connectDb = require('./config/database')
connectDb();
const etudiantRouter = require('./routes/etudiants');
app.use('/students', etudiantRouter);
const presenceRouter = require('./routes/presence')
app.use('/presence', presenceRouter)
app.listen(port, () => {
    console.log(`server listen on port : ${port}`);
})